import javax.management.relation.RoleList;
import java.lang.reflect.Array;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.function.Consumer;


public class Main {
    public static void main(String[] args) {
        //object not initialized
        Main.objNotInit();
        Main.outOfBound();

        Main.numberFormatEx();
        Main.classCastEx();
        Main.classNotFound();
        Main.illegalEx();
        Main.illegalState();
        Main.unsupportedOperation();
    }

    public static void objNotInit(){
        try {
            Object Test = null;
            Test.hashCode();
        }
        catch (Exception ex){
            System.out.println("Throwing a null point exception");
            System.out.println(ex.getClass());
        }
    }

    public static void outOfBound(){
        try{
            List<Integer> list = new ArrayList<Integer>();
            list.add(-1, 45);
        }
        catch (Exception e){
            System.out.println(e.getClass());
        }
    }

    public static void numberFormatEx(){
        try{
            String number = "not number";
            Integer t = Integer.parseInt(number);
        }
        catch (NumberFormatException ex){
            System.out.println("NumberFormatException catched");
        }

    }

    public static void classCastEx(){
        try{
            Object test = "test";
            ArrayList out = (ArrayList) test;
        }
        catch (ClassCastException ex){
            System.out.println(ex.getClass());
        }

    }

    public static void classNotFound(){
        try {
            Class.forName("NewClass");

        }catch(ClassNotFoundException ex){
            System.out.println(ex.getClass());
        }
    }

    /**
     * Почему не генерится
     */
    public static void illegalEx(){
        List t = new RoleList();
       /* try{
            Main.test(t);
        }catch (IllegalArgumentException ex){
            System.out.println(ex.getCause());
        }*/
    }

    public static void test(ArrayList s){
        System.out.println(s);
    }
    //************************************************

    //IllegalStateException
    public static void illegalState(){
        try{
            FlyBuilder bus = new FlyBuilder();
            bus.fly();
        }
        catch (IllegalStateException ex){

            System.out.println(ex.getMessage());
        }

    }

   // UnsupportedOperationException
    public static void unsupportedOperation(){
        try{
            Finder f = new Finder();
            f.remove();
        }
        catch(UnsupportedOperationException e){
            System.out.println(e.getClass());
        }
    }

}

class FlyBuilder{
    private Integer fnumber = null;
        public void setFlightNumber(Integer flightNumber){
            this.fnumber = flightNumber;
        }

        public void fly(){
            if(this.fnumber == null){
                throw new IllegalStateException("The FNumber is not specified yet.");
            }
        }

}

class Finder implements Iterator{
    public ArrayList list;
    public void setBanch(ArrayList list){
        this.list = list;
    }

    public String next(Integer i){
        return (String) this.list.get(i);
    }

    @Override
    public boolean hasNext() {
        return false;
    }

    @Override
    public Object next() {
        return null;
    }


    @Override
    public void forEachRemaining(Consumer action) {

    }
}